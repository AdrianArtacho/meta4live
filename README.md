![META_logo](https://bitbucket.org/AdrianArtacho/meta4live/raw/HEAD/META_logo.png)

# META4Live

[Objects](https://bitbucket.org/AdrianArtacho/workspace/projects/META) within the *META* provide a live performance workflow for Ablenton Live. This environement is developed and mantained by [Adrian Artacho](http://www.artacho.at/).





| Name                                                                  | Description |
| --------------------------------------------------------------------- | ----------- |
| [cc-decoder](https://bitbucket.org/AdrianArtacho/cc-decoder/)         |             |
| [cc-encoder](https://bitbucket.org/AdrianArtacho/cc-encoder/)         |             |
| [meta_apckey25](https://bitbucket.org/AdrianArtacho/meta_apckey25/)   |             |
| [meta_checklist](https://bitbucket.org/AdrianArtacho/meta_checklist/) |             |
| [meta_cue](https://bitbucket.org/AdrianArtacho/meta_cue/)             |             |
| [meta_display](https://bitbucket.org/AdrianArtacho/meta_display/)     |             |
| [meta_keys](https://bitbucket.org/AdrianArtacho/meta_keys/)           |             |
| [meta_knob](https://bitbucket.org/AdrianArtacho/meta_knob/)           |             |
| [meta_knobhub](https://bitbucket.org/AdrianArtacho/meta_knobhub/)     |             |
| [meta_launcher](https://bitbucket.org/AdrianArtacho/meta_launcher/)   |             |
| [meta_nano](https://bitbucket.org/AdrianArtacho/meta_nano/)           |             |
| [meta_notes](https://bitbucket.org/AdrianArtacho/meta_notes/)         |             |
| [meta_pages](https://bitbucket.org/AdrianArtacho/meta_pages/)         |             |
| [meta_reaktor](https://bitbucket.org/AdrianArtacho/meta_reaktor/)     |             |
| [meta_scene](https://bitbucket.org/AdrianArtacho/meta_scene/)         |             |
| [meta_tempo](https://bitbucket.org/AdrianArtacho/meta_tempo/)         |             |
| [meta_timer](https://bitbucket.org/AdrianArtacho/meta_timer/)         |             |
